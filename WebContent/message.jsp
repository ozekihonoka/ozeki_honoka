<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<a href="./">ホーム</a>
		</div>
		<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
		<div class="form-area">
			<form action="message" method="post">
				<label for="title">件名</label>
					<input type="text" id="title" name="title" class="title-box" value="${message.title}" ><br>
				<label for="categoly">カテゴリー</label>
					<input type="text" id="categoly" name="category" class="categoly-box" value="${message.category}"><br>
				<label for="text">投稿内容</label>
					<textarea name="text" id="text" rows="10" class="text-box">${message.text}</textarea><br>
				<button type="submit">投稿</button>
			</form>
		</div>
	</div>
	<div class="copyright"> Copyright(c)Ozeki honoka</div>
</body>
</html>