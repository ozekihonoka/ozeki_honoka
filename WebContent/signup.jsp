<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                        <c:remove var="errorMessages" />
                    </ul>
                </div>
            </c:if>

			<!-- 確認用パスワードある点に注意 -->
			<div>
				<a href="management">ユーザー管理</a>
	            <form action="signup" method="post"><br />
	                <label for="account">アカウント</label>
	                <input name="account" id="account" value="${user.account}"/><br />

	                <label for="password">パスワード</label>
	                <input name="password" type="password" id="password" /> <br />

	                <label for="checkPassword">確認用パスワード</label>
	                <input name="checkpassword" type="password" id="checkpassword" /> <br />

	                <label for="name">名前</label>
	                <input name="name" id="name" value="${user.name}"/> <br />

	                <label for="branchId">支社</label>
	                	<select id="branchId" name="branchId">
	                		<option value="0">選択してください</option>
			                	<c:forEach items="${branches}" var="branch">
			                		<c:if test="${branch.id == user.branchId}">
				                		<option value="${branch.id}"  selected>${branch.name}</option>
				                 	</c:if>
				                 	<c:if test="${branch.id != user.branchId}">
				                		<option value="${branch.id}">${branch.name}</option>
				                	 </c:if>
				             	</c:forEach>
		             	</select><br>


	                <label for="departmentId">部署</label>
	                	<select id="departmentId" name="departmentId">
	                		<option value="0">選択してください</option>
			               		<c:forEach items="${departments}" var="department">
			                		<c:if test="${department.id == user.departmentId}">
				                 		<option value="${department.id}" selected>${department.name}</option>
				                 	</c:if>
				                 	<c:if test="${department.id != user.departmentId}">
				                		<option value="${department.id}">${department.name}</option>
				                	</c:if>
				             	</c:forEach>
		             	</select><br>

	                <input type="submit" value="登録" /> <br />

	           	 </form>
			</div>
            <div class="copyright">Copyright(c)Ozeki Honoka</div>
        </div>
    </body>
</html>
