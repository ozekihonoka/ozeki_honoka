<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="vieport" content="width=device-width">
        <title>ホーム画面</title>
        <link rel="stylesheet" href="./css/style.css" type="text/css">
	</head>
    <body>
        <div class="main-contents">
        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                        <c:remove var="errorMessages"/>
                    </ul>
                </div>
            </c:if>
            <div class="header">
                <a href="message">新規投稿</a>
                <c:if test="${loginUser.departmentId == 1}">
                	<a href="management">ユーザー管理</a>
                </c:if>
                <a href="logout">ログアウト</a>
            </div>
            <div class="messages">
            	<!-- 投稿絞り込み機能 -->
            	<form action="index.jsp" method="get">
            		<input type="date" name="startDate" value="${startDate}">
            		～<input type="date" name="endDate" value="${endDate}">
            		<input type="text" name="category" value="${category}">
            		<button type="submit">検索</button>
            	</form>
            	<!-- データベースにある投稿一覧をひとつずつ出力 -->
    			<c:forEach items="${messages}" var="message">
        			<div class="message">
        				<div class="name">
            				<c:out value="${message.name}" />
            			</div>
            			<div class="title">
                			<c:out value="${message.title}" />
                		</div>
                		<div class="category">
                			<c:out value="${message.category}" />
                		</div>
                		<div class="text">
                			<pre><c:out value="${message.text}" /></pre>
                		</div>
                		<div class="date">
                			<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
                		</div>
        			</div>
        			<form action="deleteMessage" method="post">
	        			<div class="messageDeleteBox">
	        				<c:if test="${message.userId == loginUser.id}">
	        					<button name="id" onclick="return confirm('削除します。よろしいですか？')" value="${message.id}">削除</button>
	        				</c:if>
	        			</div>
	        		</form>
        			<!-- 今出力している投稿に対するコメントを出力。-->
	        		<c:forEach items="${comments}" var="comment">
	        			 <c:if test="${comment.messageId == message.id}">
		        			<div class="name">
		        				<c:out value="${comment.name}" />
		        			</div>
		        			<div class="text">
		        				<pre><c:out value="${comment.comment}" /></pre>
		        			</div>
		        			<div class="date">
		        				<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
		        			</div>

			        		 <form action="deleteComment" method="post">
		        			 	<div class="commentDeleteBox">
		        					<c:if test="${comment.userId == loginUser.id}">
		        						<button name="id" onclick="return confirm('削除します。よろしいですか？')" value="${comment.id}">削除</button>
		        					</c:if>
		        				</div>
		        			</form>
		        		</c:if>
	        		</c:forEach>
        			<form action="comment" method="post">
	        			<div class="comment">
	        				<input type="hidden" name="id" value="${message.id}">
	        				<label for="text">コメント入力欄</label>
							<textarea name="comment" id="comment" rows="10" class="text-box" ></textarea>
							<button type="submit" value="${message.id}">コメント投稿</button>
	        			</div>
	        		</form>
    			</c:forEach>
			</div>
            <div class="copyright"> Copyright(c)Ozeki honoka</div>
        </div>
    </body>
</html>