<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>管理画面</title>
	</head>
	<body>
		<div class="main-contents">
        	<c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="errorMessage">
                            <li><c:out value="${errorMessage}" />
                        </c:forEach>
                        <c:remove var="errorMessages"/>
                    </ul>
                </div>
         	</c:if>
         	<div class="header">
                <a href="./">ホーム</a>
                <a href="signup">ユーザー新規登録</a>
            </div>
         	<div class="users">
            	<!-- ユーザー一覧の表示 -->
    			<c:forEach items="${userBranchDepartments}" var="userBranchDepartment">
        			<div class="message">
        				<div class="acount">
            				<c:out value="${userBranchDepartment.account}" />
            			</div>
            			<div class="userName">
                			<c:out value="${userBranchDepartment.userName}" />
                		</div>
                		<div class=".branchName">
                			<c:out value="${userBranchDepartment.branchName}" />
                		</div>
                		<div class="departmentName">
                			<c:out value="${userBranchDepartment.departmentName}" />
                		</div>
                		<!-- 0だったら稼働中、１だったら停止中を表示する -->
                		<div class="isStopped">
                			<c:if test="${userBranchDepartment.isStopped == 0}">
                				<p>稼働中</p>
                			</c:if>
                			<c:if test="${userBranchDepartment.isStopped == 1}">
                				<p>停止中</p>
                			</c:if>
                		</div>
                		<!-- 編集ボタンを押す際にどのユーザーの編集なのか分かるよう、ユーザーの情報を一緒に送信 -->
                		<form action="setting" method="get">
                			<input type="hidden" value="${userBranchDepartment.id}" name="userId">
                			<button type="submit">編集</button>
                		</form>

                		<!-- 現在ログインしている人事部のユーザーのユーザー情報については、ボタンを表示させないようにする。
                		ユーザー稼働状態だったら停止ボタンの表示 -->
                		<c:if test="${loginUser.id != userBranchDepartment.id}">
	                		<c:if test="${userBranchDepartment.isStopped == 0}">
		                		<form action="stop" method="post">
		                			<input type="hidden" value="${userBranchDepartment.id}" name="userId">
		                			<input type="hidden" value="1" name="isStopped">
		                			<button name="stop" onclick="return confirm('停止します。よろしいですか？')">停止</button>
		                		</form>
		                	</c:if>
	                	</c:if>

	                	<c:if test="${loginUser.id != userBranchDepartment.id}">
	                		<c:if test="${userBranchDepartment.isStopped == 1}">
		                		<form action="stop" method="post">
		                			<input type="hidden" value="${userBranchDepartment.id}" name="userId">
		                			<input type="hidden" value="0" name="isStopped">
		                			<button name="stop" onclick="return confirm('復活します。よろしいですか？')">復活</button>
		                		</form>
		                	</c:if>
		                </c:if>


        			</div>
        		</c:forEach>
        	</div>
         </div>
	</body>
</html>