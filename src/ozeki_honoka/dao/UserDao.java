package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.User;
import ozeki_honoka.exception.SQLRuntimeException;

public class UserDao {
	//ユーザーの登録情報をデータベースにインサートするメソッド
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    is_stopped, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // branch_id
            sql.append("    ?, ");									//department_id
            sql.append("    0, ");									//is_stopped 登録時は絶対に稼働なので0で登録。
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログイン情報をもとにデータベースからセレクトしてくる。
    public User select(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ?  AND password = ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, account);
            ps.setString(2, password);


            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            //ログイン情報が間違っていたらnullを返す
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            //ログイン情報と一致したらそのユーザーの情報をBeansに格納して返す。
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //新規登録、またはユーザー更新の際に、すでに同じアカウント名がないか確認する。
    //重複がなければnull、あればそのユーザーを一つ返す。
    public User select(Connection connection, String account) {

    	  PreparedStatement ps = null;
    	  try {
    	    String sql = "SELECT * FROM users WHERE account = ?";

    	    ps = connection.prepareStatement(sql);
    	    ps.setString(1, account);

    	    ResultSet rs = ps.executeQuery();

    	    List<User> users = toUsers(rs);
    	    //なかったらnullで返す。
    	    if (users.isEmpty()) {
    	      return null;
    	    //以下はあり得ない。ので画面上でユーザー向けにエラーメッセを出す必要はない
    	    } else if (2 <= users.size()) {
    	      throw new IllegalStateException("ユーザーが重複しています");
    	    //すでに１個あった場合は登録できない。
    	    } else {
    	      return users.get(0);
    	    }
    	  } catch (SQLException e) {
    	    throw new SQLRuntimeException(e);
    	  } finally {
    	    close(ps);
    	  }
    	}

    //IDをもとにユーザー情報をとってくるメソッド。編集用
    //ログイン情報をもとにデータベースからセレクトしてくる。
    public User select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ? ";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else if(users.size() == 1) {
                return users.get(0);
            } else {
            	return null;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setBranchId(rs.getInt("branch_id"));
                user.setDepartmentId(rs.getInt("department_id"));
                user.setIsStopped(rs.getInt("is_stopped"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    //ユーザーの更新情報をデータベースにアップデートするメソッド。
    //アカウント稼働or停止状態はユーザー更新の際にはいじらない。
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE  users SET  ");
            sql.append("    account = ?, ");
            //もしパスワードが設定さえていた場合は以下の一文を入れる。
			if(!(user.getPassword().isEmpty())) {
				sql.append("    password = ?, ");
			}
            sql.append("    name = ?, ");
            sql.append("    branch_id = ?, ");
            sql.append("    department_id = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("    WHERE id= ? ");
            //sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            //パスワードが新たに設定されているか否かで処理を分ける。
            ps.setString(1, user.getAccount());
            if(!(user.getPassword().isEmpty())) {
            	ps.setString(2, user.getPassword());
            	ps.setString(3, user.getName());
                ps.setInt(4, user.getBranchId());
                ps.setInt(5, user.getDepartmentId());
                ps.setInt(6, user.getId());  //更新対象のユーザーのID
            }else {
	            ps.setString(2, user.getName());
	            ps.setInt(3, user.getBranchId());
	            ps.setInt(4, user.getDepartmentId());
	            ps.setInt(5, user.getId());  //更新対象のユーザーのID
            }

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザーの稼働と停止状態を書き換えるメソッド
    public void userOperation(Connection connection, int isStopped, int userId) {

        PreparedStatement ps = null;
        try {
            String sql = "UPDATE users SET is_stopped = ?  WHERE id = ? ";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, isStopped);
            ps.setInt(2, userId);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}
