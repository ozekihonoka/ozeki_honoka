package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.UserMessage;
import ozeki_honoka.exception.SQLRuntimeException;


public class UserMessageDao {

	//ホームに表示する投稿一覧を取り出す。WHEREで条件の追加を行う。
	//サービスが始まる前の日時と現在の日時を条件に入れる。
    public List<UserMessage> select(Connection connection, String startDate, String endDate, String category) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    messages.id as id, ");  //メッセージのID
            sql.append("    messages.title as title, ");  //件名
            sql.append("    messages.text as text, ");  //投稿文
            sql.append("    messages.category as category, ");  //カテゴリー
            sql.append("    messages.user_id as user_id, ");  //投稿者ID
            sql.append("    messages.created_date as created_date, ");  //投稿日時
            sql.append("    users.name as name ");  //ユーザーテーブルに登録されている名前
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");  //メッセージテーブルの投稿者IDとユーザーテーブルのIDは同じなので結び付ける。
            sql.append("WHERE  messages.created_date BETWEEN ? ");
            sql.append("AND ? ");
            if(category != null) {
            	sql.append("AND messages.category like ? ");
            }
            sql.append("ORDER BY created_date DESC ");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, startDate);
            ps.setString(2, endDate);
            if(category != null) {
            	ps.setString(3,  "%" + category + "%");
            }





            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	//変数messageはUserMessage。ホームに表示するためのメッセージをmessages(List)に格納。
                UserMessage message = new UserMessage();
                message.setId(rs.getInt("id"));  //投稿の番号とコメントを紐づけるために投稿番号が必要
                message.setName(rs.getString("name"));
                message.setTitle(rs.getString("title"));
                message.setCategory(rs.getString("category"));
                message.setUserId(rs.getInt("user_id"));
                message.setText(rs.getString("text"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}