package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ozeki_honoka.beans.Message;
import ozeki_honoka.exception.SQLRuntimeException;

public class MessageDao {

  public void insert(Connection connection, Message message) {

    PreparedStatement ps = null;
    try {
      StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("    title, ");
            sql.append("    text, ");
            sql.append("    category, ");
            sql.append("    user_id, ");  //投稿した人のID。ユーザーテーブルより。
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // title
            sql.append("    ?, ");                                  // text
            sql.append("    ?, ");                                  // categoly
            sql.append("    ?, ");                                  // user_id
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

      ps = connection.prepareStatement(sql.toString());

      ps.setString(1, message.getTitle());
      ps.setString(2, message.getText());
      ps.setString(3, message.getCategory());
      ps.setInt(4, message.getUserId());

      ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  //特定の投稿を削除するメソッド。投稿のIDを引数で受け取ってその投稿を消す。
  public void delete(Connection connection, int messageId) {

	    PreparedStatement ps = null;
	    try {
	      StringBuilder sql = new StringBuilder();
	      	sql.append("DELETE FROM messages  ");
	      	sql.append("WHERE id = ?; ");



	      ps = connection.prepareStatement(sql.toString());
	      ps.setInt(1, messageId);
	      ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	  }

}