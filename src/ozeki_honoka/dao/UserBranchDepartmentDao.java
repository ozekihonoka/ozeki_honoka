package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.UserBranchDepartment;
import ozeki_honoka.exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	//ユーザーの一覧をとってくるメソッド
	public List<UserBranchDepartment> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    users.id as id, ");  //ユーザーのID。
            sql.append("    users.account as account, ");  //アカウント名。
            sql.append("    users.name as userName, ");  //ユーザーテーブルに登録されている名前
            sql.append("    users.branch_id as branch_id, "); //ユーザーテーブルに登録されている支社番号。紐づけるために取ってくる。
            sql.append("    users.department_id as department_id, "); //ユーザーテーブルに登録されている部署番号。紐づけるために取ってくる。
            sql.append("    branches.name as branchName, ");  //支社テーブルに登録されている支社の名前
            sql.append("    departments.name as departmentName, ");  //部署テーブルに登録されている支社の名前
            sql.append("    users.is_stopped as is_stopped, ");  //ユーザー停止状態
            sql.append("	users.created_date as created_date");
            sql.append("	FROM users ");
            sql.append("	INNER JOIN branches ");
            sql.append("	ON users.branch_id = branches.id ");
            sql.append("	INNER JOIN departments ");
            sql.append("	ON users.department_id = departments.id ");
            sql.append("	ORDER BY created_date ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartment(rs);
            return userBranchDepartments;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	//編集したいユーザーのみ取り出すメソッド。
		public UserBranchDepartment select(Connection connection, int id) {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("    users.id as id, ");  //ユーザーのID。
	            sql.append("    users.account as account, ");  //アカウント名。
	            sql.append("    users.name as userName, ");  //ユーザーテーブルに登録されている名前
	            sql.append("    users.branch_id as branch_id, "); //ユーザーテーブルに登録されている支社番号。紐づけるために取ってくる。
	            sql.append("    users.department_id as department_id, "); //ユーザーテーブルに登録されている部署番号。紐づけるために取ってくる。
	            sql.append("    branches.name as branchName, ");  //支社テーブルに登録されている支社の名前
	            sql.append("    departments.name as departmentName, ");  //部署テーブルに登録されている支社の名前
	            sql.append("    users.is_stopped as is_stopped, ");  //ユーザー停止状態
	            sql.append("	users.created_date as created_date");
	            sql.append("	FROM users ");
	            sql.append("	INNER JOIN branches ");
	            sql.append("	ON users.branch_id = branches.id ");
	            sql.append("	INNER JOIN departments ");
	            sql.append("	ON users.department_id = departments.id ");
	            sql.append("	WHERE  users.id = ? ");
	            sql.append("	ORDER BY created_date ");

	            ps = connection.prepareStatement(sql.toString());
	            ps.setInt(1, id);

	            ResultSet rs = ps.executeQuery();

	            List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartment(rs);
	            return userBranchDepartments.get(0);
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

	private List<UserBranchDepartment> toUserBranchDepartment(ResultSet rs) throws SQLException {

        List<UserBranchDepartment> userBranchDepartments = new ArrayList<UserBranchDepartment>();
        try {
            while (rs.next()) {

            	UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
            	userBranchDepartment.setId(rs.getInt("id"));
            	userBranchDepartment.setAccount(rs.getString("account"));
            	userBranchDepartment.setUserName(rs.getString("userName"));
            	userBranchDepartment.setBranchName(rs.getString("branchName"));
            	userBranchDepartment.setDepartmentName(rs.getString("departmentName"));
            	userBranchDepartment.setIsStopped(rs.getInt("is_stopped"));

                userBranchDepartments.add(userBranchDepartment);
            }
            return userBranchDepartments;
        } finally {
            close(rs);
        }
    }
}
