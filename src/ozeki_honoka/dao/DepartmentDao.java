package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.Department;
import ozeki_honoka.exception.SQLRuntimeException;

public class DepartmentDao {

	//データベースから部署情報をとってくる。
    public List<Department> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM departments";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            List<Department> departments = toDepartment(rs);

            return departments;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //部署情報を格納する
    private List<Department> toDepartment(ResultSet rs) throws SQLException {

        List<Department> departments = new ArrayList<Department>();
        try {
            while (rs.next()) {
            	Department department = new Department();
            	department.setId(rs.getInt("id"));
            	department.setName(rs.getString("name"));


            	departments.add(department);
            }
            return departments;
        } finally {
            close(rs);
        }
    }
}


