package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.UserComment;
import ozeki_honoka.exception.SQLRuntimeException;

public class UserCommentDao {
	//コメントは新しい順に取り出す
    public List<UserComment> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    comments.id as id, ");  //コメントのID
            sql.append("    comments.text as text, ");
            sql.append("    comments.user_id as user_id, ");  //コメントした人のID。ユーザーテーブルより。
            sql.append("    comments.message_id as message_id, ");  //コメントを付けている投稿のID
            sql.append("    users.name as name, ");  //コメントを投稿した人の名前
            sql.append("    comments.created_date as created_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            List<UserComment> comments = toUserComment(rs);
            return comments;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //コメントをリストに入れて返す。
    private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

        List<UserComment> comments = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
            	UserComment comment = new UserComment();
            	comment.setId(rs.getInt("id"));
            	comment.setMessageId(rs.getInt("message_id"));
            	comment.setUserId(rs.getInt("user_id"));
            	comment.setName(rs.getString("name"));
            	comment.setComment(rs.getString("text"));
            	comment.setCreatedDate(rs.getTimestamp("created_date"));

            	comments.add(comment);
            }
            return comments;
        } finally {
            close(rs);
        }
    }
}