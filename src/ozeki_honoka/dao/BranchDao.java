package ozeki_honoka.dao;

import static ozeki_honoka.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ozeki_honoka.beans.Branch;
import ozeki_honoka.exception.SQLRuntimeException;

public class BranchDao {

	//データベースから支社情報をとってくる。
    public List<Branch> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            List<Branch> branches = toBranches(rs);

            return branches;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //支社情報を格納する
    private List<Branch> toBranches(ResultSet rs) throws SQLException {

        List<Branch> branches = new ArrayList<Branch>();
        try {
            while (rs.next()) {
            	Branch branch = new Branch();
            	branch.setId(rs.getInt("id"));
            	branch.setName(rs.getString("name"));


            	branches.add(branch);
            }
            return branches;
        } finally {
            close(rs);
        }
    }
}
