package ozeki_honoka.beans;

import java.io.Serializable;

//支社情報を格納する
public class Branch implements Serializable {

    private int id;  //支社ID
    private String name;  //支社名


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =name;
    }
}

