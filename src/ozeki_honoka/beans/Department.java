package ozeki_honoka.beans;

import java.io.Serializable;

//部署情報を格納する
public class Department implements Serializable {

    private int id;  //部署ID
    private String name;  //部署名


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name =name;
    }
}

