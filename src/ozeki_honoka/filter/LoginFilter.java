package ozeki_honoka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ozeki_honoka.beans.User;

//未ログイン状態でログインページ以外のリクエストが来た場合、ログインページに飛ばす。
@WebFilter("/*")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	//子クラスのメソッドが使いたいのでキャスト。
    	HttpServletResponse httpServletResponse =  (HttpServletResponse)response;
    	HttpServletRequest httpServletRequest = (HttpServletRequest)request;

    	//Servletのパスがログインページか否かで処理を分ける。
    	String path = httpServletRequest.getServletPath();

    	//ログインしているか否かフィルターで確認する。
    	HttpSession session = httpServletRequest.getSession();
    	User user = (User) session.getAttribute("loginUser");
    	List<String> errorMessages = new ArrayList<String>();

    	if(user == null && !(path.equals("/login"))) {
    		errorMessages.add("ログインしてください");
    		session.setAttribute("errorMessages", errorMessages);
    		httpServletResponse.sendRedirect("login");
    		return;
    	}


        // サーブレットを実行
        chain.doFilter(request, response);

        System.out.println("EncodingFilter# chain.doFilterが実行されました。");
    }

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void destroy() {
    }

}