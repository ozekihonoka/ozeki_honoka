package ozeki_honoka.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ozeki_honoka.beans.User;


@WebFilter(urlPatterns = { "/management", "/signup", "/setting" })
public class ManagementFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

    	//子クラスのメソッドが使いたいのでキャスト。
    	HttpServletResponse httpServletResponse =  (HttpServletResponse)response;
    	HttpServletRequest httpServletRequest = (HttpServletRequest)request;

    	//ユーザーの部署が人事総務部か否か確認する。
    	HttpSession session = httpServletRequest.getSession();
    	User user = (User) session.getAttribute("loginUser");
    	int departmentId = user.getDepartmentId();
    	int branchId = user.getBranchId();

    	List<String> errorMessages = new ArrayList<String>();

    	if(departmentId != 1) {
    		errorMessages.add("権限がありません");
    		session.setAttribute("errorMessages", errorMessages);
    		httpServletResponse.sendRedirect("./");
    		return;
    	}else if(branchId != 1) {
    		errorMessages.add("権限がありません");
    		session.setAttribute("errorMessages", errorMessages);
    		httpServletResponse.sendRedirect("./");
    		return;

    	}




        // サーブレットを実行
        chain.doFilter(request, response);

        System.out.println("EncodingFilter# chain.doFilterが実行されました。");
    }

    @Override
    public void init(FilterConfig config) {
    }

    @Override
    public void destroy() {
    }

}