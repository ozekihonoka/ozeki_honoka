package ozeki_honoka.service;

import static ozeki_honoka.utils.CloseableUtil.*;
import static ozeki_honoka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ozeki_honoka.beans.Department;
import ozeki_honoka.dao.DepartmentDao;

public class DepartmentService {

	//DepartmentDaoから支社情報をとってくる
		public List<Department> select() {

	        Connection connection = null;
	        try {

	            connection = getConnection();
	            List<Department> departments = new DepartmentDao().select(connection);
	            commit(connection);

	            return departments;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

}


