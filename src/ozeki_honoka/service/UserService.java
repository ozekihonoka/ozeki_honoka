package ozeki_honoka.service;

import static ozeki_honoka.utils.CloseableUtil.*;
import static ozeki_honoka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ozeki_honoka.beans.User;
import ozeki_honoka.beans.UserBranchDepartment;
import ozeki_honoka.dao.UserBranchDepartmentDao;
import ozeki_honoka.dao.UserDao;
import ozeki_honoka.utils.CipherUtil;

public class UserService {

	//ユーザー新規登録のメソッド。引数でユーザーの登録情報が格納されたbeansを受け取る。
    public void insert(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().insert(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //引数でログイン情報を受け取って、パスワードを暗号化してDaoに渡す。
    public User select(String account, String password) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(password);

            connection = getConnection();
            User user = new UserDao().select(connection, account, encPassword);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public User select(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, id);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

  //登録、または更新したいユーザーの＠アカウントをUerDaoに取ってきてもらう。
    public User select(String account) {

        Connection connection = null;
        try {
            connection = getConnection();
            User user = new UserDao().select(connection, account);
            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    //管理画面で一覧を表示させるためにユーザーの情報一覧をDaoに取ってきてもらう。
    //ログインの際のセレクトメソッドをオーバーロードしている。
    public List<UserBranchDepartment> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            //ホーム画面に表示するためのコメント一覧をリストに格納
            List<UserBranchDepartment> userBranchDepartments = new UserBranchDepartmentDao().select(connection);
            commit(connection);

            return userBranchDepartments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //編集したいユーザーのみ取ってくるメソッド。
    public UserBranchDepartment selectSettingUser(int id) {

        Connection connection = null;
        try {
            connection = getConnection();
            //ホーム画面に表示するためのコメント一覧をリストに格納
            UserBranchDepartment userBranchDepartment = new UserBranchDepartmentDao().select(connection,  id);
            commit(connection);

            return userBranchDepartment;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ユーザー情報更新のメソッド。引数でユーザーの更新情報が格納されたbeansを受け取る。
    public void update(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
        	if(!user.getPassword().isEmpty()) {
	            // パスワード暗号化
	            String encPassword = CipherUtil.encrypt(user.getPassword());
	            user.setPassword(encPassword);
        	}

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

  //ユーザー稼働停止状態を書き換えるDaoに行くまでに経由するメソッド。
    public void userOperation(int isStopped, int userId) {

        Connection connection = null;
        try {

            connection = getConnection();
            new UserDao().userOperation(connection, userId, isStopped);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
