package ozeki_honoka.service;

import static ozeki_honoka.utils.CloseableUtil.*;
import static ozeki_honoka.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ozeki_honoka.beans.Message;
import ozeki_honoka.beans.UserMessage;
import ozeki_honoka.dao.MessageDao;
import ozeki_honoka.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String start, String end, String category) {
    	//Daoに開始の日時と終了日時を引数で渡す。
    	//最初にデフォルト値を設定。
    	String startDate = "2021-01-01 00:00:00";
    	//現在時刻を取り出す
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = dateFormat.format(date);
    	String endDate = strDate;

    	if(!(StringUtils.isEmpty(start))) {
    		startDate = start + " 00:00:00";
    	}

    	if(!(StringUtils.isEmpty(end))) {
    		endDate = end + " 23:59:59";
    	}

    	if(StringUtils.isEmpty(category)) {
    		category = null;
    	}

        Connection connection = null;
        try {
            connection = getConnection();
            //ホーム画面に表示するための投稿一覧をリストに格納
            List<UserMessage> messages = new UserMessageDao().select(connection, startDate, endDate, category);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(int messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().delete(connection, messageId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
