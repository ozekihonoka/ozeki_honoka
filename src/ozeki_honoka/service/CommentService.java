package ozeki_honoka.service;

import static ozeki_honoka.utils.CloseableUtil.*;
import static ozeki_honoka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ozeki_honoka.beans.Comment;
import ozeki_honoka.beans.UserComment;
import ozeki_honoka.dao.CommentDao;
import ozeki_honoka.dao.UserCommentDao;

public class CommentService {

    public void insert(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().insert(connection, comment);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserComment> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            //ホーム画面に表示するためのコメント一覧をリストに格納
            List<UserComment> comments = new UserCommentDao().select(connection);
            commit(connection);

            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントを削除するために経由するメソッド
    public void delete(int commentId) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().delete(connection, commentId);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}