package ozeki_honoka.service;

import static ozeki_honoka.utils.CloseableUtil.*;
import static ozeki_honoka.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import ozeki_honoka.beans.Branch;
import ozeki_honoka.dao.BranchDao;

public class BranchService {

	//BrancheDaoから支社情報をとってくる
	public List<Branch> select() {

        Connection connection = null;
        try {

            connection = getConnection();
            List<Branch> branches = new BranchDao().select(connection);
            commit(connection);

            return branches;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
