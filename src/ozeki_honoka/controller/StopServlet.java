package ozeki_honoka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ozeki_honoka.service.UserService;


@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//稼働停止状態を変更したいユーザーのIDを格納。management.jspより。
    	int userId = Integer.parseInt(request.getParameter("userId"));
    	int isStopped = Integer.parseInt(request.getParameter("isStopped"));

    	new UserService().userOperation(userId, isStopped);
    	response.sendRedirect("management");
    }
}


