package ozeki_honoka.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ozeki_honoka.beans.UserBranchDepartment;
import ozeki_honoka.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //管理画面に表示させたいユーザー一覧の取得。
        List<UserBranchDepartment> userBranchDepartments = new UserService().select();

        request.setAttribute("userBranchDepartments", userBranchDepartments);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }


}