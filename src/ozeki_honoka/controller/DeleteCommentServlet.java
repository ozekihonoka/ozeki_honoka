package ozeki_honoka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ozeki_honoka.service.CommentService;


@WebServlet(urlPatterns = { "/deleteComment" })
public class DeleteCommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //home.jspから消したいコメントのIDを受け取ってきてそれをCommentService → CommentDaoに渡して消してもらう。
    	int commentId = Integer.parseInt((request.getParameter("id")));
    	new CommentService().delete(commentId);
    	response.sendRedirect("./");

    }


}