package ozeki_honoka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ozeki_honoka.beans.Message;
import ozeki_honoka.beans.User;
import ozeki_honoka.service.MessageService;

@WebServlet(urlPatterns = { "/message" })
public class MessageServlet extends HttpServlet {

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("message.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//誰が投稿したかわかるようにログインしているユーザーの情報をとってくる。
    	HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");

        List<String> errorMessages = new ArrayList<String>();

        Message message = getMessage(request, user);

        if (!isValid(message, errorMessages)) {
        	request.setAttribute("message", message);
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("message.jsp").forward(request, response);
            return;
        }

        message.setUserId(user.getId());

        new MessageService().insert(message);
        response.sendRedirect("./");
    }

    //投稿内容を格納して返す。
    private Message getMessage(HttpServletRequest request, User user) throws IOException, ServletException {

    	Message message = new Message();
    	message.setUserId(user.getId());
    	message.setTitle(request.getParameter("title"));
    	message.setCategory(request.getParameter("category"));
    	message.setText(request.getParameter("text"));
        return message;
    }

    private boolean isValid(Message message, List<String> errorMessages) {

    	String title = message.getTitle();
    	String category = message.getCategory();
    	String text = message.getText();

    	if (StringUtils.isBlank(title)) {
            errorMessages.add("件名を入力してください");
        } else if (30 < title.length()) {
            errorMessages.add("件名は30文字以下で入力してください");
        }

    	if (StringUtils.isBlank(category)) {
            errorMessages.add("カテゴリーを入力してください");
        } else if (10 < category.length()) {
            errorMessages.add("カテゴリーは10文字以下で入力してください");
        }

        if (StringUtils.isBlank(text)) {
            errorMessages.add("本文を入力してください");
        } else if (1000 < text.length()) {
            errorMessages.add("本文は1000文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}