package ozeki_honoka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ozeki_honoka.beans.User;
import ozeki_honoka.service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");

        User user = new UserService().select(account, password);
        List<String> errorMessages = new ArrayList<String>();
        int isStopped = 0;
        if(user != null) {
        	isStopped= user.getIsStopped();
        }

        //もしエラーがあった場合、入力情報を送り返し表示させる
        if (!isValid(user, errorMessages, account, password, isStopped)) {
        	request.setAttribute("account", account);
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    private boolean isValid(User user, List<String> errorMessages, String password, String account, int isStopped) {

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        }

        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }

        if(errorMessages.size() == 0) {
	        if (user == null) {
	            errorMessages.add("アカウントまたはパスワードが間違っています。");
	        }

	    	if (isStopped == 1) {
	            errorMessages.add("ログインに失敗しました");
	        }
        }

    	if (errorMessages.size() != 0) {
            return false;
        }


        return true;
    }

}
