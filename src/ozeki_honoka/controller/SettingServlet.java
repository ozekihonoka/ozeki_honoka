package ozeki_honoka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ozeki_honoka.beans.Branch;
import ozeki_honoka.beans.Department;
import ozeki_honoka.beans.User;
import ozeki_honoka.service.BranchService;
import ozeki_honoka.service.DepartmentService;
import ozeki_honoka.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//編集画面で、アドレスバーのIDに不正なパラメータが入力された際エラーメッセージを格納する。
    	List<String> errorMessages = new ArrayList<String>();
    	HttpSession session = request.getSession();

    	//uesrIdを取り出してくる。
    	String userId = request.getParameter("userId");

    	User user = null;
    	//idが空白でない、かつ数字
    	if(!(StringUtils.isBlank(userId)) && userId.matches("^[0-9]*$")) {
    		//編集したいユーザーの情報をとってくる。
        	user = new UserService().select(Integer.parseInt(userId));

    	}

    	if( user == null) {
    		errorMessages.add("不正なパラメータが入力されました");
    		session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("management");
            return;
    	}

		List<Branch> branches = getBranchs();
    	List<Department> departments = getDepartments();

    	request.setAttribute("branches", branches);
    	request.setAttribute("departments", departments);
    	request.setAttribute("user", user);
    	request.getRequestDispatcher("setting.jsp").forward(request, response);


    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {


    	String checkPassword = request.getParameter("checkpassword");
        List<String> errorMessages = new ArrayList<String>();


        User user = new User();
        user = getUser(request);
        User firstUser = new UserService().select(user.getAccount());
        List<Branch> branches = getBranchs();
    	List<Department> departments = getDepartments();
    	int settingUserId = 0;
    	if(firstUser != null) {
    		settingUserId = firstUser.getId();
    	}

        if (!isValid(user, errorMessages, checkPassword, firstUser, settingUserId)) {
        	request.setAttribute("user", user);
        	request.setAttribute("branches", branches);
        	request.setAttribute("departments", departments);
            request.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }
        new UserService().update(user);
        //処理が正常に完了したらユーザー管理画面へ遷移
        response.sendRedirect("management");
    }

   //支社リストをとってくるメソッド。
    private List<Branch> getBranchs() throws IOException, ServletException {

    	List<Branch> branches = new BranchService().select();
        return branches;
    }

  //部署リストをとってくるメソッド。
    private List<Department> getDepartments() throws IOException, ServletException {

    	List<Department> departments = new DepartmentService().select();
        return departments;
    }

    //編集ページで入力された情報をbeanに入れる。
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("userId")));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
        user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages, String checkPassword, User firstUser, int settingUserId) {

    	String account = user.getAccount();
        String password = user.getPassword();
        String name = user.getName();
        int userId = user.getId();

        int branchId = user.getBranchId();
        int departmentId = user.getDepartmentId();

        if (StringUtils.isEmpty(name) || 10 < name.length()) {
            errorMessages.add("名前は10文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account) || !(account.matches("^[0-9a-zA-Z]{6,20}$"))) {
            errorMessages.add("アカウント名は6文字以上20文字以下、半角英数字で入力してください");
        }
        if(firstUser != null && userId != settingUserId) {
        	errorMessages.add("アカウントが重複しています");
        }

        if (!(StringUtils.isEmpty(password))) {

	        if (!(password.matches("^[0-9a-zA-Z -/:-@\\[-\\`\\{-\\~]{6,20}$"))) {
	        	errorMessages.add("パスワードは6文字以上20文字以下、半角文字で入力してください");
			}
        }

        if(!password.equals(checkPassword)) {
        	errorMessages.add("確認用パスワードと一致しません。");
        }
        if(branchId == 0) {
        	errorMessages.add("支社を選択してください");
        }
        if(departmentId == 0) {
        	errorMessages.add("部署を選択してください");
        }
        if(branchId == 1 && (departmentId == 3 || departmentId == 4)) {

        	errorMessages.add("支社と部署の組み合わせが不正です");

        }else if(branchId != 1 && (departmentId == 1 || departmentId == 2)){
        	errorMessages.add("支社と部署の組み合わせが不正です");

        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;


    }

}
