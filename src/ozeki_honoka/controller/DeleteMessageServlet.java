package ozeki_honoka.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ozeki_honoka.service.MessageService;


@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //home.jspから消したい投稿のIDを受け取ってきてそれをMessageService → MessageDaoに渡して消してもらう。
    	int messageId = Integer.parseInt((request.getParameter("id")));
    	new MessageService().delete(messageId);
    	response.sendRedirect("./");

    }


}