package ozeki_honoka.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import ozeki_honoka.beans.Comment;
import ozeki_honoka.beans.User;
import ozeki_honoka.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//誰がコメントしたか登録するためにログインユーザーの情報が必要。
    	HttpSession session = request.getSession();
        User user = (User) session.getAttribute("loginUser");

        List<String> errorMessages = new ArrayList<String>();

        Comment comment = getComment(request, user);

        if (!isValid(comment, errorMessages)) {
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
            return;
        }
        new CommentService().insert(comment);
        response.sendRedirect("./");

    }

    private Comment getComment(HttpServletRequest request, User user) throws IOException, ServletException {

        Comment comment = new Comment();
        comment.setComment(request.getParameter("comment"));
        comment.setUserId(user.getId());
        comment.setMessageId(Integer.parseInt(request.getParameter("id")));
        return comment;
    }

    private boolean isValid(Comment comment, List<String> errorMessages) {

    	String text = comment.getComment();
        if (StringUtils.isBlank(text)) {
            errorMessages.add("コメントを入力してください");
        } else if (500 < text.length()) {
            errorMessages.add("500文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}