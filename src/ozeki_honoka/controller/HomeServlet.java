package ozeki_honoka.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ozeki_honoka.beans.UserComment;
import ozeki_honoka.beans.UserMessage;
import ozeki_honoka.service.CommentService;
import ozeki_honoka.service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {



        //ホーム画面に表示したい投稿一覧とそれに対するコメントをとってくる。
        //カレンダーから受け取った値を引数にしてMessageServletを呼び出す。
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String category = request.getParameter("category");

        List<UserMessage> messages = new MessageService().select(startDate, endDate, category);
        List<UserComment> comments = new CommentService().select();

        request.setAttribute("comments", comments);
        request.setAttribute("messages", messages);
        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", endDate);
        request.setAttribute("category", category);
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }


}